[🏠 Praktikum Basis Data](https://gitlab.com/marchgis/march-ed/2023/courses/praktikum-basis-data/)

# Data Definition Language

[[_TOC_]]

## Struktur Informasi di Database

```mermaid
flowchart TD
    A[Server Database] -->|memiliki| B[Database]
    B -->|memiliki| C[Table / Tabel]
    C -->|memiliki| D[Row / Baris / Record]
    D -->|memiliki| E[Column / Kolom / Attribute / Field]
```

Server DATABASE dapat memiliki sejumlah DATABASE, dimana setiap DATABASE dapat mewakili kumpulan informasi yang saling terkait pada satu proses bisnis. Contohnya DATABASE perkuliahan yang didalamnya terdapat data mahasiswa, dosen, kelas, krs, dll.

Setiap DATABASE dapat memiliki banyak TABLE, dimana setiap TABLE mewakili informasi dengan objek / entitas dan atribut yang sama. Contohnya TABLE kuda mewakili data kuda yang terdapat di peternakan.

TABLE memiliki sejumlah data, yang diwakili oleh ROW atau baris data. Contohnya pada TABLE kuda, terdapat lima ekor kuda yang diwakili oleh lima ROW. Tiap ROW memiliki FIELD yang berisi atribut data. Pada contoh di bawah ini, ada empat FIELD yaitu ID, Nama, Jenis, dan Umur.

ID | Nama | Jenis | Umur
--- | --- | --- | ---
1 | Si Coklat | Kuda Arab | 14 tahun
2 | Si Hitam | Kuda Himalaya | 17 tahun
3 | Si Putih | Kuda Texas | 21 tahun
4 | Si Oren | Kuda Soreang | 10 tahun
5 | Si Belang | Kuda Skandinavia | 13 tahun

<!-- https://mermaid.live/ -->

## Apa itu Query, Apa itu SQL ?
Query adalah perintah yang digunakan untuk berinteraksi dengan DATABASE. Salah satu bahasa Query yang populer diterapkan di banyak vendor basis data relasional adalah SQL. SQL adalah standar bahasa yang digunakan berbagai vendor database untuk query basis data relasional. Walaupun begitu, dialek SQL antar vendor database contohnya MySQL, PostgreSQL, SQL Server, ada perbedaan walaupun perintah umumnya sama.

## Apa itu Data Definition Language (DDL) ?
DDL adalah kumpulan Query yang dapat digunakan untuk membentuk struktur data baru di DATABASE. Penggunaan paling umum dari DDL adalah untuk membuat TABLE.

## Syntax Pembuatan Table

### Aturan SQL Untuk DDL Secara Umum
```sql
CREATE TABLE <nama_tablenya_apa> (
    <nama_fieldnya_apa> <tipe_datanya_apa> <konfigurasi_fieldnya_gimana>,
    <nama_fieldnya_apa> <tipe_datanya_apa> <konfigurasi_fieldnya_gimana>
);
```

### Penambahan FOREIGN KEY Pada Table
```sql
-- a = Nama TABLE anak
ALTER TABLE a         
-- b = Nama constraint foreign key nya (bebas, biasanya nama_tabel_anak_nama_tabel_ortu_fk)      
ADD CONSTRAINT b
-- c = Nama COLUMN dari TABLE anak (a.c) yang akan dikaitkan ke TABLE orang tua (rujukan)
FOREIGN KEY (c) 
-- d = Nama TABLE orang tua (rujukan)
-- e = Nama COLUMN dari TABLE orang tua (d.e) yang akan dirujuk dari COLUMN TABLE anak
REFERENCES d(e)
-- f = Berisi Keyword Referential Actions yaitu CASCADE | SET NULL | RESTRICT | NO ACTION | SET DEFAULT 
ON DELETE f
```

### Contoh Implementasi DDL di MySQL
```sql
CREATE TABLE kuda (
    -- id adalah nama COLUMN nya
    -- INT adalah tipe datanya : INTEGER / bilangan. Tipe data INT umum ada di berbagai vendor database
    -- AUTO_INCREMENT agar data baru otomatis diisi id nya secara inkremental. AUTO_INCREMENT keyword khusus di MySQL
    -- PRIMARY KEY artinya COLUMN id ini bertindak sebagai PRIMARY KEY
    id INT AUTO_INCREMENT PRIMARY KEY,
    -- nama adalah nama COLUMN nya
    -- VARCHAR(40) adalah tipe data yang berisi huruf dan angka, maksimum 40 karakter
    -- NOT NULL artinya COLUMN ini tidak boleh kosong (harus diisi)
    nama VARCHAR(40) NOT NULL,
    jenis VARCHAR(20) NOT NULL,
    umur INT NOT NULL,
    -- kisah_perekrutan adalah nama COLUMN nya
    -- TEXT adalah tipe data yang berisi teks yang panjang
    -- Tidak disebut NOT NULL artinya COLUMN ini boleh NULL / tidak memiliki nilai
    kisah_perekrutan TEXT
); 
```

## Tipe Data
### Beberapa Tipe Data Populer di MySQL
Tipe Data | Deskripsi | Contoh Penggunaan
--- | --- | ---
VARCHAR(N) | Rangkaian karakter yang merepresentasikan teks, dengan besaran N. | Nama orang, nama perusahan, email, nomor telepon
TEXT(N) | | Cerita, paragraf, berita
INT | Bilangan bulat contohnya 234 | Berat badan, tinggi badan, jumlah, data kategori, id
FLOAT | Bilangan desimal contohnya 3.1434 | Jarak tempuh, kecepatan, 
DATE | Tanggal dalam format 2023-03-31 | Tanggal lahir, tanggal acara
DATETIME | Tanggal dan waktu dalam format 2023-03-31 19:30:00 | Waktu mulai, waktu selesai, waktu kirim, waktu absensi


## Challenge
Dengan menggunakan Query DDL, buatlah struktur data untuk keseluruhan objek / entitas yang mewakili produk digitalmu !
