# Memilih dan Menampilkan Data Dengan Data Query Language (DQL)

## SELECT
SELECT merupakan perintah untuk memilih, membuat dan menampilkan data dari tabel di database ke dalam bentuk tabel
```mermaid
graph TD

    1["TABLE A"] --> 2["SELECT"]
    3["TABLE B"] --> 2
    4["Nilai C"] --> 2
    2 --> 5["TABLE Hasil"]
```

## Contoh Data
Berikut ini script MySQL berisi contoh data penduduk dan kode kabupaten yang didalamnya terdapat perintah DDL untuk membuat tabelnya serta DML untuk menambahkan data sampelnya.
- [Contoh data penduduk.sql](./penduduk.sql)
- [Contoh data kode-kabupaten.sql](./kode-kabupatens.sql)

Perintah tersebut dapat dijalankan di database MySQL yang Kamu install ataupun di platform coba-coba MySQL online seperti [SQLiteonline.com](https://sqliteonline.com)

---

## Penggunaan Dasar SELECT
```mermaid
graph TD

    1("SELECT") --> 2("NAMA_COLUMN 1")
    1("SELECT") -->|COLUMN mana saja ?| 3("NAMA_COLUMN 2")
    1("SELECT") --> 4("NAMA_COLUMN N")
    2 --> 5["FROM"]
    3 --> 5
    4 --> 5
    5 -->|dari TABLE mana ?| 6["NAMA_TABLE X"]
    6 -->|yang kondisinya bagaimana ?| 7["WHERE"]
    7 --> 8["KONDISI 1"]
    7 --> 9["KONDISI 2"]
    7 --> 10["KONDISI N"]
    8 --> 11["TABLE HASIL"]
```

---

### 🦘 {+ Menampilkan Hanya Nilai Sebagai TABLE +}
```sql
SELECT 
    18 AS jumlah_mobil_teslaku,
    'Bandung' AS tempat_tinggalku,
    163 AS cabang_mixueku
```
Penjelasan: Menampilkan **TABLE** dengan **COLUMN** **jumlah_mobil_teslaku**, **tempat_tinggalku** dan **cabang_mixueku** yang didalamnya terdapat satu **ROW** data bernilai **18**, **'Bandung'** dan **163**. Perintah **AS** dapat digunakan sebagai **ALIAS** dari nama **COLUMN**.

---

### 🦘 {+ Menampilkan Sebagian COLUMN Dari TABLE +}
```sql
SELECT 
    nama_lengkap,
    gender
FROM penduduk
```
Penjelasan: Menampilkan seluruh **ROW** data dari **COLUMN** **nama_lengkap** dan **gender** dari **TABLE** penduduk. Keyword **FROM** digunakan untuk memilih **TABLE** yang akan ditampilkan datanya.

---

### 🦘 {+ Menampilkan Seluruh COLUMN Dari TABLE +}
```sql
SELECT * 
FROM penduduk
```
Penjelasan: Perintah di atas menampilkan seluruh **ROW** data dari seluruh **COLUMN** dari **TABLE** **penduduk** Tanda **\*** digunakan untuk menampilkan seluruh **COLUMN**.

---

### 🦘 {+ Memilih ROW Data Berdasarkan Kondisi Tertentu Dengan Keyword WHERE +}
**WHERE** digunakan untuk memilih kondisi dari **ROW** data yang akan ditampilkan.

```sql
SELECT *
FROM penduduk
WHERE gender = 'M'
AND ijazah_terakhir > 4
```
Penjelasan: Perintah di atas memilih **ROW** data dengan **gender** sama dengan 'M' dan **ijazah_terakhir** lebih besar dari 4. Keyword **AND** merupakan keyword untuk menggabungkan lebih dari satu perintah. Dalam hal ini **AND** digunakan untuk menggabungkan dua kondisi **WHERE**.

Operator Kondisi | Deskripsi
--- | ---
**=** | Sama dengan. Untuk perbandingan string, maka string yang dibandingkan harus persis sama.
**\>** | A Lebih besar dari B
**\>=** | A Lebih besar sama dengan B
**\<** | A Lebih kecil dari B
**\<=** | A Lebih kecil sama dengan B
**\<\>** | A Tidak sama dengan B
**\!\=** | A Tidak sama dengan B
**LIKE** | Membandingkan string A berdasarkan pola B
IN | A Ada di dalam array B
BETWEEN | A ada di atnara nilei B dan C

### 🦘 {+ Mengurutkan Hasil SELECT Dengan Keyword ORDER +}

```mermaid
graph TD

    1("SELECT") -->|COLUMN mana saja ?| 3("COLUMN 1, COLUMN 2, ..., COLUMN N")
    3 --> 5["FROM"]
    5 -->|dari TABLE mana ?| 6["NAMA_TABLE X"]
    6 -->|yang kondisinya bagaimana ?| 7["WHERE"]
    7 --> 9["CONDITION 1, CONDITION 2, ..., CONDITION N"]
    9 -->|diurutkan berdasarkan apa ?| 11["ORDER BY"]
    11 --> 12["COLUMN 1 / COLUMN 2 / COLUMN N"]
    12 -->|diurutkan A ke Z ?| 13["ASC"]
    12 -->|diurutkan Z ke A ?| 14["DESC"]
    13 --> 15["TABLE HASIL"]
    14 --> 15
```

```sql
SELECT *
FROM penduduk
ORDER BY tanggal_lahir ASC
```
Penjelasan: Perintah di atasa mengurutkan hasil berdasarkan **COLUMN** **tanggal_lahir** dari yang lebih awal hingga yang lebih akhir.


```sql
SELECT *
FROM penduduk
ORDER BY gender DESC, tanggal_lahir ASC
```
Penjelasan: Perintah di atasa mengurutkan hasil berdasarkan **COLUMN** **gender** terlebih dahulu dari Z ke A, selanjutnya **tanggal_lahir** dari yang lebih awal hingga yang lebih akhir.

## Challenge
1. Coba perintah DQL menggunakan data contoh [penduduk.sql](./penduduk.sql)
2. Buat data dummy untuk project yang dibuat menggunakan DML INSERT
3. Implementasikan perintah-perintah DQL pada data dari project yang dibuat

