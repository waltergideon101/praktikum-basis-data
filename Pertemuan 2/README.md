# Pertemuan 2 - Praktikum Basis Data

## Tools
- DB Designer
    - [diagrams.net](https://www.diagrams.net/)
    - [dbdiagram.io](https://dbdiagram.io)
        - [DBML](https://www.dbml.org/docs/#project-definition)
- Database
    - MySQL
    - [SQL Online (opsional bila instalasi terkendala)](https://sqliteonline.com/)
- DB explorer: Dbeaver

## DBML - Database Markup Language
### Definisi
- Script open source
- mendefinisikan
- mendokumentasikan
- skema dan struktur
- database relasional
- secara mudah
- dan tidak bergantung vendor database nya

Manfaat DBML:
- Memudahkan memvisualisasikan skema dan struktur database
- Memudahkan untuk mengerti atribut dari tiap tabel
- Script SQL data definition language yang ada dianggap terlalu rumit
- Dapat diaplikasikan untuk berbagai jenis vendor database relasional

### Penggunaan dbdiagram.io
[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/J-24THDAz3E/0.jpg)](https://www.youtube.com/watch?v=J-24THDAz3E)

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/watch?v=J-24THDAz3E" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->


### Contoh Sintaks DBML dan Hasil Export SQL nya

**DBML :**
```dbml
Table bajak_laut {
  id_bajak_laut integer [primary key]
  nama varchar
  asal varchar
}

Table kru {
  id_kru integer [primary key]
  id_bajak_laut integer
  nama varchar
  id_jabatan integer
  nilai_buronan integer
}

Table ref_jabatan {
  id_jabatan integer [primary key]
  nama varchar
}

Ref {
  ref_jabatan.id_jabatan < kru.id_jabatan
}

Ref {
  bajak_laut.id_bajak_laut < kru.id_bajak_laut
}

```

**Hasil SQL (MySQL) :**
```sql
CREATE TABLE `bajak_laut` (
  `id_bajak_laut` integer PRIMARY KEY,
  `nama` varchar(255),
  `asal` varchar(255)
);

CREATE TABLE `kru` (
  `id_kru` integer PRIMARY KEY,
  `id_bajak_laut` integer,
  `nama` varchar(255),
  `id_jabatan` integer,
  `nilai_buronan` integer
);

CREATE TABLE `ref_jabatan` (
  `id_jabatan` integer PRIMARY KEY,
  `nama` varchar(255)
);

ALTER TABLE `kru` ADD FOREIGN KEY (`id_jabatan`) REFERENCES `ref_jabatan` (`id_jabatan`);

ALTER TABLE `kru` ADD FOREIGN KEY (`id_bajak_laut`) REFERENCES `bajak_laut` (`id_bajak_laut`);
```

## Tips Mengkonversi Proses Bisnis Menjadi Database
- Tentukan Entitas / Objek yang wajib terlibat dalam proses bisnis tersebut, diurut sesuai prioritasnya, contoh:
    - Dalam proses bisnis perkuliahan, objek yang ada didalamnya :
        - Mahasiswa
        - Kelas
        - Pertemuan
        - Materi
        - Dosen

## Kaidah Penamaan Dalam Perancangan Database
- Konsisten dalam penamaan
    - Misal, umum ke khusus
        - binatang_kandang
        - binatang_pakan_herbal
    - Disingkat, disingkat, tidak, tidak
- Tidak ada spasi, diganti underline
- Huruf kecil
- Jangan gunakan kata kunci yang telah ada digunakan di vendor database tersebut
    - Misal select, from, title, user, lock, table

## Challenge
> **Narator:** Selamat ! hari ini Kamu diterima magang di salah satu perusahaan berbasis teknologi digital terbesar di dunia

> **Narator: 4** bulan kedepan Kamu harus menguasai kemampuan dasar Business Analysis, Data Architecture, Data Engineering, Database Administrator dan Data Analysis yang didemonstrasikan dalam bentuk perancangan arsitektur, rekayasa, dan analisis basis data terkait

**Pilih salah satu kasus di bawah ini**, dengan menggunakan dbdiagram.io / dbml, rancang skema dan struktur database untuk aplikasi:
- Aplikasi pemesanan makanan online (Gojek / Grab / Shopee / lainnya)
- Instagram
- Youtube
- Google Drive
- Ruangguru
- Aplikasi travel (Traveloka / Agoda / Airbnb / lainnya)
- Aplikasi Mobile Banking (Bebas pilih bank nya)
- Game online (Mobile Legend / PUPG / lainnya) 

