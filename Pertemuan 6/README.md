# GROUP dan JOIN

## Sample Data
- [Penduduk](https://gitlab.com/marchgis/march-ed/2023/courses/praktikum-basis-data/-/blob/main/Pertemuan%205/penduduk.sql)
- [Kode Kabupaten](https://gitlab.com/marchgis/march-ed/2023/courses/praktikum-basis-data/-/blob/main/Pertemuan%205/kode-kabupaten.sql)

## Latihan
```sql
-- Pertanyaan 1 : Berapa jumlah penduduk di jawa barat berdasarkan gender ?
SELECT
	count(*) as total_penduduk,
	gender
FROM penduduk
GROUP BY gender
```

```sql
-- Pertanyaan 2 : Berapa jumlah penduduk di jawa barat berdasarkan kode kabupaten dan gender, diurutkan berdasrkan kode kabupaten dan gender ?
SELECT
	kode_kabupaten,
	gender,
    count(*) as total_penduduk
FROM penduduk
GROUP BY gender, kode_kabupaten
ORDER BY kode_kabupaten, gender
```

```sql
-- Pertanyaan 3: Top 3 Ijazah terbanyak di wilayah ini apa sih ?
SELECT
	CASE 
    	WHEN ijazah_terakhir = 0 then 'Tidak Sekolah'
        WHEN ijazah_terakhir = 1 THEN 'SD/MI'
        WHEN ijazah_terakhir = 2 THEN 'SMP/MTS'
        WHEN ijazah_terakhir = 3 THEN 'SMA/MA/SMK'
        WHEN ijazah_terakhir = 4 THEN 'D1'
        WHEN ijazah_terakhir = 5 THEN 'D2'
        WHEN ijazah_terakhir = 6 THEN 'D3'
        WHEN ijazah_terakhir = 7 THEN 'S1/D4'
        WHEN ijazah_terakhir = 8 THEN 'S2'
        WHEN ijazah_terakhir = 9 THEN 'S3'
	ELSE 'Tidak diketahui' END as ijazah_terakhir_deskripsi,
    COUNT(*) AS jumlah_penduduk
FROM penduduk
GROUP BY ijazah_terakhir
ORDER BY COUNT(*) DESC
LIMIT 3
```

```sql
-- Pertanyaan 4: Urutkan Kabupaten berdasarkan jumlah penduduk terbanyak !
SELECT
	kk.deskripsi AS nama_kabupaten,
    COUNT(*) as jumlah_penduduk
FROM kode_kabupaten kk 
LEFT JOIN penduduk p 
	ON p.kode_kabupaten = kk.kode
GROUP BY kk.kode
ORDER BY jumlah_penduduk DESC
```

```sql
-- Pertanyaan 5: Tentukan topologi dari kabupaten berdasarkan jumlah penduduknya, ambil 10 tertinggi !
select
	'32' as kode_provinsi,
	kode_kabupaten,
	count(*) AS total_penduduk,
    case when count(*) >= 8 then 'KOTA BESAR' ELSE 'KOTA KECIL' END as topologi
from penduduk
Group by kode_kabupaten
order by total_penduduk DESC
limit 10
```

## Tools
- [SQLiteOnline](https://sqliteonline.com/)

## Referensi
- [Group By - W3Schools](https://www.w3schools.com/mysql/mysql_groupby.asp)
- [Group By - MySQLTutorial](https://www.mysqltutorial.org/mysql-group-by.aspx)
- [Group By - Devart](https://blog.devart.com/mysql-group-by-tutorial.html)
- [Join - W3Schools](https://www.w3schools.com/mysql/mysql_join.asp)
- [Join - MySQLTutorial](https://www.mysqltutorial.org/mysql-join/)
- [Join - Cheatsheet](https://adiyatmubarak.wordpress.com/2016/01/09/457/)
