# IF214002 - Praktikum Basis Data

## Learning Outcome Outline
- Mampu mendemonstrasikan perancangan dan implementasi basis data relasional

## Pertemuan

No | Learning Outcome | Materi & Asesmen
---|---|---
1 | <ol><li>Mampu menjelaskan berbagai prosedur instalasi dan konfigurasi basis data relasional di industri</li><li> Mampu menginstalasi dan konfigurasi basis data relasional</li><li>Mampu menginstalasi dan menggunakan tools eksplorasi dan query database</li></ol> | [Materi](./Pertemuan 1%20)
2 | <ol><li>Mampu mendemonstrasikan perancangan tabel menggunakan tools</li><li>Mampu mendemonstrasikan penggunaan syntax Data Definition Language (DDL) untuk pembuatan tabel pada basis data relasional</li></ol> | [Materi](./Pertemuan 2)
3 | <ol><li>Mampu mendemonstrasikan penggunaan syntax Data Definition Language (DDL) untuk pembuatan tabel pada basis data relasional</li><li>Mampu mendemonstrasikan pemilihan tipe data Table Field dan konfigurasi Table yang tepat pada tahap Data Definition Language (DDL)</li></ol> | [Materi](./Pertemuan 3)
4 | <ol><li>Mampu mendemonstrasikan penggunaan syntax Data Manipulation Language (DML) untuk manipulasi data di dalam tabel pada basis data relasional</li></ol> | informatika.digital
5 | <ol><li>Mampu mendemonstrasikan penggunaan syntax Data Query Language (DQL) untuk memilih dan menampilkan data dari dalam tabel pada basis data relasional</li></ol> | [Materi](./Pertemuan 5)
6 | Mampu mendemonstrasikan penggunaan syntax DQL Group dan Join | [Materi](./Pertemuan 5)
7 | |
 | UTS |
8 | |
9 | |
10 | |
12 | |
13 | |
14 | |
 | UAS |

## Tools
- MySQL
- PostgreSQL
- Dbeaver

## Referensi
- 

### Materi
- 


### Instalasi Tools
- 

### Kurikulum
- 
